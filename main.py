import digitemp

# press CTRL+C to exit loop

if __name__ == '__main__':
    tsensor = digitemp.DS18B20()
    while True:
        try:
            tsensor.refresh()
            print("DegF: " + str(tsensor.degF) + ", DegC: " + str(tsensor.degC))
            
        except KeyboardInterrupt:
            quit()
