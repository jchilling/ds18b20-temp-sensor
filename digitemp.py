import os


class DS18B20():
    """ This module reads the DS18B20 temperature sensor in degC and degF.

    Wire the data (yellow) to pin GPIO 4 (this pin is read as the default) with a 4.7kOhm pullup resistor between DATA (yellow) and VCC (red). I wired the red to the 3.3V pin, but the datasheets say 3.3-5V is okay. Wire the blue to GND.
    
    To configure, do "sudo nano /boot/config.txt".

    Add the text "dtoverlay=w1-gpio" to the bottom of the file. (To do a pin other than the default, do "dtoverlay=w1-gpio, gpiopin=18" for GPIO 18, for example.

    Save and close. Do "sudo reboot".

    The device should now be writing a textfile into a folder in the directory /sys/bus/w1/devices/ that is parsed with the function read().

    The folder in this directory that the file is written to is '28-' and the serial number of the device.

    The serial number allows multiple DS18B20s to be read at the same time. However, this module is set up to only read one DS18B20 at a time.

    """
    
    def __init__(self, degC='', degF='', serialNum=''):
        self.degC = degC
        self.degF = degF
        self.serialNum = self.sensor()
        self.degC = self.read(self.serialNum)['degC']
        self.degF = self.read(self.serialNum)['degF']
        
    def sensor(self):
        for dir_name in os.listdir('/sys/bus/w1/devices'):
            if dir_name.startswith('28-'):
                ds18b20 = dir_name
        return ds18b20

    
    def read(self, ds18b20):
        location = '/sys/bus/w1/devices/' + ds18b20 + '/w1_slave'
        tfile = open(location)
        text = tfile.read()
        tfile.close()
        secondline = text.split("\n")[1]
        temperaturedata = secondline.split(" ")[9]
        temperature = float(temperaturedata[2:])
        celsius = temperature / 1000
        fahrenheit = (celsius * 1.8) + 32
        return {'degC': celsius, 'degF': fahrenheit}
    
    def refresh(self):
        self.read(self.serialNum)

    
        

"""example"""
##tsensor = DS18B20()
##print(tsensor.degC)
##print(tsensor.degF)
